---
Title: "22. #everyonecancontribute cafe: Multi-tenancy with Kiosk in Kubernetes"
Date: 2021-03-24
Aliases: []
Tags: ["gitlab","hetzner","cloud","terraform","ansible","kubernetes","security","kiosk","multi-tenancy"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

We are learning how to deploy and secure Kubernetes into Hetzner cloud in this series:

- 14. cafe: Provisioned the server and agent VMs with Terraform and Ansible [in the first session](/post/2021-01-27-cafe-14-kubernetes-deployments-to-hetzner-cloud/) with [Max](https://twitter.com/ekeih).
- 15. cafe: Deployed [k3s as Kubernetes distribution](/post/2021-02-03-cafe-15-kubernetes-deployments-to-hetzner-cloud-part-2/) with [Max](https://twitter.com/ekeih).
- 16. cafe: Learned about pods and the [Hetzner load balancer](/post/2021-02-10-cafe-16-kubernetes-deployments-to-hetzner-cloud-part-3/) with [Max](https://twitter.com/ekeih).
- 17. cafe: [Ingress controller for load balancer cost savings](/post/2021-02-17-cafe-17-kubernetes-deployments-to-hetzner-cloud-part-4/) with [Max](https://twitter.com/ekeih).
- 18. cafe: [Get to know Kubernetes user authentication and authorization with RBAC](/post/2021-02-24-cafe-18-kubernetes-rbac-authentication-authorization/) with [Niclas Mietz](https://twitter.com/solidnerd).
- 19. cafe: [Break into Kubernetes Security](/post/2021-03-03-cafe-19-break-into-kubernetes-security/) with [Philip Welz](https://twitter.com/philip_welz).
- 20. cafe: [Securing Kubernetes with Kyverno](/post/2021-03-10-cafe-20-securing-kubernetes-with-kyverno/) with [Philip Welz](https://twitter.com/philip_welz).
- 21. cafe: [ Secure Kubernetes with OpenID](/post/2021-03-17-cafe-21-kubernetes-security-openid-kiosk/) with [Niclas Mietz](https://twitter.com/solidnerd).

In this session, we install Kiosk into an existing Kubernetes cluster.

* The Kubernetes cluster was prepared before the session, deployed with KubeOne/Terraform.
* Install Kiosk with Helm
* Kiosk account is similar to role binding
* Create [spaces](https://github.com/loft-sh/kiosk#3-working-with-spaces), and let Kiosk manage the required resources
* `kubectl api-resource --namespaced` to see API resources requiring a namespace definition in #kubernetes 💡
* Create [deletable spaces](https://github.com/loft-sh/kiosk#35-create-deletable-spaces)
* Replicate certificates between Kubernetes namespaces with [Kubed](https://github.com/appscode/kubed) 🏗
* John wants to create multiple namespaces. We can limit him to only create 2 as quota. [Quota management in multi-tenancy environments](https://github.com/loft-sh/kiosk#4-setting-account-limits).
* Unlimited compute resources are the default. Again, quota management with [resource and account quotas](https://github.com/loft-sh/kiosk#42-accountquotas) - allow only 2 pods, but request 3 - prohibited. 🔥 

In the future, we'll explore more Kubernetes topics:

- Automate our Kubernetes setup so that everyone can contribute :)
- CI/CD, IaC and GitOps with the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/)
- Hetzner [storage volumes](https://github.com/hetznercloud/csi-driver)
- Monitoring with Prometheus, GitLab CI/CD deployments and much more :) 

### Insights

- [Miro board](https://miro.com/app/board/o9J_lTOCVrg=/)
- [Kubernetes group repos](https://gitlab.com/everyonecancontribute/kubernetes)
- [Repository with the Kubernetes cluster](https://gitlab.com/everyonecancontribute/kubernetes/k3s-demo)
- [Twitter thread](https://twitter.com/dnsmichi/status/1374769483911430144)


### Recording

Enjoy the session! 🦊 

{{< youtube S46d9wkx6kU >}}









