
---
Title: "7. Kaeffchen: Terraform and GitLab Live Demo and DevSecOps Culture Talk"
Date: 2020-07-01
Aliases: []
Tags: ["terraform","gitlab","devsecops","culture"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### General

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/17)
- Guests: Niclas Mietz, Mario Kleinsasser, Bernhard Rausch, Michael Aigner, Nico Meisenzahl, Marcel Weinberg, Michael Friedrich

### Highlights

- Niclas kicked off with a live demo on _Managing AWS credentials with Terraform using the [GitLab integration](https://docs.gitlab.com/ee/user/infrastructure/) (state backend, repository and MR integration)_. Must watch if you want to start learning! A blog post is coming [soon](https://gitlab.com/everyonecancontribute/everyonecancontribute.gitlab.io/-/issues/24).
- Bernhard [continued](https://twitter.com/rauschbit/status/1277136034430627845?s=19) our discussion about _Ops/DevOps and how our working/thinking changed the past years_. Thanks Patrick Debois for starting! 

### Recording

Enjoy the session!

{{< youtube FYx3Quq-WAQ >}}
