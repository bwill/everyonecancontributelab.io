---
Title: "40. #EveryoneCanContribute cafe: Terraform and Helm Registries in GitLab"
Date: 2021-07-28
Aliases: []
Tags: ["terraform","gitlab","helm","kubernetes"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

[Niclas Mietz](https://twitter.com/solidnerd) walks us through a Terraform module with Civocloud deployments, published to the GitLab registry. [Philip Welz](https://twitter.com/philip_welz) takes over.

Reminder: GitLab Commit Virtual happens next week. [Register now!](https://gitlabcommitvirtual2021.com/)

### Recording

Enjoy the session! 🦊  

{{< youtube XkAC15DDx4Y >}}

<br>

### Highlights

The [civo-k3s repository](https://gitlab.com/everyonecancontribute/kubernetes/civo-k3s) provides a minimal set of Terraform modules to provision a Civo Cloud Kubernetes cluster [learned last week](/post/2021-07-21-cafe-39-civo-cloud-k3s-gitlab/). Niclas creates the `.gitlab-ci.yml` configuration for Terraform, and uses additional scripts and jobs to package and push the Terraform module to the GitLab registy. An example module will invoke the runtime test in [examples/main.tf](https://gitlab.com/everyonecancontribute/kubernetes/civo-k3s/-/blob/main/examples/main.tf). 

Recommended for beginners: Start with 2 Git repositories, one as "producer" of the module, and one "consumer" which tests and installs the module. Refactor the Infrastructure code later into a monorepo when applicable.

Philip took over in the same repository, using the podtato head demo with a Helm chart. It got packaged, pushed to the GitLab Helm repository, and the application gets deployed using the `helm` cli command. 

### Insights

- Documentation
  - [Terraform Registry](https://docs.gitlab.com/ee/user/packages/terraform_module_registry/)
  - [Helm Chart Repository](https://docs.gitlab.com/ee/user/packages/helm_repository/)
  - [Untracked Artifacts in CI/CD](https://docs.gitlab.com/ee/ci/yaml/#artifactsuntracked)
- Blog
  - [GitLab Helm Registry](https://about.gitlab.com/blog/2021/07/26/gitlab-helm-package-registry/)  
- [civo-k3s repository](https://gitlab.com/everyonecancontribute/kubernetes/civo-k3s)
- [Twitter thread](https://twitter.com/dnsmichi/status/1420404195895689216)



