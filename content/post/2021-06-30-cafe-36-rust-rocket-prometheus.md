---
Title: "36. #EveryoneCanContribute cafe: Learn Rust with Rocket web app & Prometheus Monitoring"
Date: 2021-06-30
Aliases: []
Tags: ["rust","development","prometheus","learn","metrics"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

Learn developing a web app with Rocket.rs in Rust. Add monitoring metrics for Prometheus with rocket_prometheus.

### Recording

Enjoy the session! 🦊  

{{< youtube RAIo3SaIHjg >}}

<br>

### Highlights

We decided to go with [Rocket.rs Getting Started](https://rocket.rs/v0.5-rc/guide/getting-started/) and learn how to register handles with the web app. We also learned about managing [Cargo dependencies](https://doc.rust-lang.org/cargo/reference/specifying-dependencies.html) and [Cargo targets](https://doc.rust-lang.org/cargo/reference/cargo-targets.html). 

After finishing the implementation, we wanted to add Prometheus Monitoring metrics with [rocket_prometheus](https://docs.rs/rocket_prometheus/0.7.0/rocket_prometheus/). Since Rocket requires Rust nightly builds, we also needed to use the [main.rs example](https://github.com/sd2k/rocket_prometheus/blob/async/examples/main.rs) from the async branch. 

We used Gitpod and the GitLab Web IDE to develop only in the browser, and allowing everyone to follow async. 

### Insights

- [Repository learn-rust](https://gitlab.com/everyonecancontribute/dev/learn-rust)
- [Rust Macros](https://doc.rust-lang.org/reference/macros.html)
- [Continuous Integration with GitLab CI/CD](https://doc.rust-lang.org/cargo/guide/continuous-integration.html)
- [Packaging a Rust web service using Docker](https://blog.logrocket.com/packaging-a-rust-web-service-using-docker/)
- [Prometheus Client in Rust](https://docs.rs/prometheus/0.12.0/prometheus/)
- [Optimizing CI/CD Pipeline for Rust Projects (Gitlab & Docker)](https://faun.pub/optimizing-ci-cd-pipeline-for-rust-projects-gitlab-docker-98df64ae3bc4)
- [Rust Tracking issue: declarative macros 2.0](https://github.com/rust-lang/rust/issues/39412)
- [Twitter thread](https://twitter.com/philip_welz/status/1410276002509512707)









