---
Title: "20. #everyonecancontribute cafe: Securing Kubernetes with Kyverno"
Date: 2021-03-10
Aliases: []
Tags: ["gitlab","hetzner","cloud","terraform","ansible","kubernetes","security"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

We are learning how to deploy and secure Kubernetes into Hetzner cloud in this series:

- 14. cafe: Provisioned the server and agent VMs with Terraform and Ansible [in the first session](/post/2021-01-27-cafe-14-kubernetes-deployments-to-hetzner-cloud/) with [Max](https://twitter.com/ekeih).
- 15. cafe: Deployed [k3s as Kubernetes distribution](/post/2021-02-03-cafe-15-kubernetes-deployments-to-hetzner-cloud-part-2/) with [Max](https://twitter.com/ekeih).
- 16. cafe: Learned about pods and the [Hetzner load balancer](/post/2021-02-10-cafe-16-kubernetes-deployments-to-hetzner-cloud-part-3/) with [Max](https://twitter.com/ekeih).
- 17. cafe: [Ingress controller for load balancer cost savings](/post/2021-02-17-cafe-17-kubernetes-deployments-to-hetzner-cloud-part-4/) with [Max](https://twitter.com/ekeih).
- 18. cafe: [Get to know Kubernetes user authentication and authorization with RBAC](/post/2021-02-24-cafe-18-kubernetes-rbac-authentication-authorization/) with [Niclas Mietz](https://twitter.com/solidnerd).
- 19. cafe: [Break into Kubernetes Security](/post/2021-03-03-cafe-19-break-into-kubernetes-security/) with [Philip Welz](https://twitter.com/philip_welz).

In this session, we change the perspective again and secure a Kubernetes cluster with [Philip Welz](https://twitter.com/philip_welz).

* Overview of Cloud Native Security - [The 4C's of Cloud Native security](https://kubernetes.io/docs/concepts/security/overview/#the-4c-s-of-cloud-native-security)
* Explore and secure the Kubernetes API 
* Secure ETCD with [encryption at REST](https://kubernetes.io/docs/tasks/administer-cluster/encrypt-data/)
* Debug API server not starting problem
  * [crictl with containerd](https://kubernetes.io/docs/tasks/debug-application-cluster/crictl/)
  * [YAML Linter](https://learnk8s.io/validating-kubernetes-yaml)
  * [Show whitespaces in vim](https://vim.fandom.com/wiki/Highlight_unwanted_spaces)
* [Kyverno](https://kyverno.io/)
  * Intercepts API requests prior persisting to ETCD as an [admission controller](https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/) with Webhooks:
    * [MutatingAdmissionWebhook](https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/#mutatingadmissionwebhook) & [ValidatingAdmissionWebhook](https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/#validatingadmissionwebhook)
  * Extends the API with [Custom Resource Definitions](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/)
  * General [Policy structure](https://kyverno.io/docs/writing-policies/structure/)  
  * Policies can be `enforced` or `audited` and will be recorded in [reports](https://kyverno.io/docs/writing-policies/background/) 
  * [Policy Reporter](https://github.com/fjogeleit/policy-reporter)

Next week, we'll explore more Kubernetes topics:

- OpenID Connection of the API Server with Dex and GitLab and multi-tenancy with [kiosk](https://github.com/loft-sh/kiosk)
- Hetzner [storage volumes](https://github.com/hetznercloud/csi-driver)
- Monitoring with Prometheus, GitLab CI/CD deployments and much more :) 

### Insights

- [Kubernetes group repos](https://gitlab.com/everyonecancontribute/kubernetes)
- [Repository with all commands from the session](https://gitlab.com/everyonecancontribute/kubernetes/k8s-security)
- [Twitter thread](https://twitter.com/dnsmichi/status/1369697355281367047)
- [Kyverno policy examples - best practices](https://kyverno.io/policies/)
- [Test-drive Kyverno with BadPods](https://github.com/BishopFox/badPods)
- [Exploring Kyverno - 3 Part Series](https://neonmirrors.net/post/2020-12/exploring-kyverno-part1/) 

### Recording

Enjoy the session! 🦊 

{{< youtube C4wgxxiJNq8 >}}







