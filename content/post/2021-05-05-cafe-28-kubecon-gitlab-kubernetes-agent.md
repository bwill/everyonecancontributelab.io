---
Title: "28. #EveryoneCanContribute cafe: KubeCon EU and GitLab Kubernetes Agent"
Date: 2021-05-05
Aliases: []
Tags: ["gitlab","kubernetes","agent","kubecon"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

We started with a recap of KubeCon EU day 1 and continued with the [GitLab Kubernetes Agent](https://about.gitlab.com/releases/2021/04/22/gitlab-13-11-released/), following up from [last week](/post/2021-04-28-cafe-27-gitlab-kubernetes-agent-gitops/) together with Niclas and Michael. 

### Recording

Enjoy the session and KubeCon EU this week! 🦊 

{{< youtube ttvuyeyDOQw >}}

<br>

### Highlights

- KubeCon EU
  - [Schedule](https://kccnceu2021.sched.com/?iframe=no)
  - [dnsmichi's schedule](https://kccnceu2021.sched.com/dnsmichi)
  - [Communication on releases and deprecations with Kat Cosgrove and Ian Coldwater](https://twitter.com/dnsmichi/status/1389906087755800576)
  - GitLab Commit at KubeCon: [Playlist](https://www.youtube.com/playlist?list=PLFGfElNsQthaOB2umyiAj4VfKDMHSk1Fu) [Viktor Nagy on GitOps](https://www.youtube.com/watch?v=kEE1EzIepak&list=PLFGfElNsQthaOB2umyiAj4VfKDMHSk1Fu&index=7)
- [Philip's Kubernetes Agent documentation MR](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/60712) 
- [Fixes and plans for the agent from Mikhail](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/60712#note_564825255) 
- [GitLab Kubernetes Agent Documentation](https://docs.gitlab.com/ee/user/clusters/agent/)
- [Kubewarden](https://docs.kubewarden.io/)

We'll continue in future sessions :)

### Insights

- Kubernetes agent
  - [Direction / Roadmap](https://about.gitlab.com/direction/configure/kubernetes_management/)
  - [Planning Epic](https://gitlab.com/groups/gitlab-org/-/epics/3329)
  - [Repository](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent)
- Demo repositories
  - [GitLab Kubernetes agent](https://gitlab.com/everyonecancontribute/kubernetes/k8s-agent)
  - [Gitpod examples](https://gitlab.com/everyonecancontribute/kubernetes/gitpod-examples)
  - [GitLab GitOps Walkthrough Demo](https://gitlab.com/gitlab-examples/ops/configure-product-walkthrough/)
- [Miro board](https://miro.com/app/board/o9J_lTOCVrg=/)
- [Kubernetes group repos](https://gitlab.com/everyonecancontribute/kubernetes)


