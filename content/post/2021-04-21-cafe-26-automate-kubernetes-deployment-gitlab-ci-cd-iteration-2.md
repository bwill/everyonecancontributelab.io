---
Title: "26. #everyonecancontribute cafe: Automate Kubernetes deployment with Terraform and GitLab CI/CD, iteration 2"
Date: 2021-04-21
Aliases: []
Tags: ["gitlab","hetzner","cloud","terraform","ansible","kubernetes","cicd","automation"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

We are learning how to deploy and secure Kubernetes into Hetzner cloud in this series:

- 14. cafe: Provisioned the server and agent VMs with Terraform and Ansible [in the first session](/post/2021-01-27-cafe-14-kubernetes-deployments-to-hetzner-cloud/) with [Max](https://twitter.com/ekeih).
- 15. cafe: Deployed [k3s as Kubernetes distribution](/post/2021-02-03-cafe-15-kubernetes-deployments-to-hetzner-cloud-part-2/) with [Max](https://twitter.com/ekeih).
- 16. cafe: Learned about pods and the [Hetzner load balancer](/post/2021-02-10-cafe-16-kubernetes-deployments-to-hetzner-cloud-part-3/) with [Max](https://twitter.com/ekeih).
- 17. cafe: [Ingress controller for load balancer cost savings](/post/2021-02-17-cafe-17-kubernetes-deployments-to-hetzner-cloud-part-4/) with [Max](https://twitter.com/ekeih).
- 18. cafe: [Get to know Kubernetes user authentication and authorization with RBAC](/post/2021-02-24-cafe-18-kubernetes-rbac-authentication-authorization/) with [Niclas Mietz](https://twitter.com/solidnerd).
- 19. cafe: [Break into Kubernetes Security](/post/2021-03-03-cafe-19-break-into-kubernetes-security/) with [Philip Welz](https://twitter.com/philip_welz).
- 20. cafe: [Securing Kubernetes with Kyverno](/post/2021-03-10-cafe-20-securing-kubernetes-with-kyverno/) with [Philip Welz](https://twitter.com/philip_welz).
- 21. cafe: [Secure Kubernetes with OpenID](/post/2021-03-17-cafe-21-kubernetes-security-openid-kiosk/) with [Niclas Mietz](https://twitter.com/solidnerd).
- 22. cafe: [Multi-tenancy with Kiosk in Kubernetes](/post/2021-03-24-cafe-22-multi-tenancy-with-kiosk-in-kubernetes/) with [Niclas Mietz](https://twitter.com/solidnerd).
- 23. cafe: [Automate our Kubernetes setup & deep dive into Hetzner firewall](/post/2021-03-31-cafe-23-automate-kubernetes-setup-hetzner-firewall-feature/) with [Max](https://twitter.com/ekeih).
- 24. cafe: [Automate Kubernetes deployment with Terraform and GitLab CI/CD](/post/2021-04-07-cafe-24-automate-kubernetes-deployment-ansible-gitlab-cicd/) with [Max](https://twitter.com/ekeih).

In this session, we automate the deployment of the Kubernetes cluster with [Max](https://twitter.com/ekeih) inside GitLab CI/CD:

* Automate the deployment from the [repository](https://gitlab.com/everyonecancontribute/kubernetes/k3s-demo) with GitLab CI/CD
* Continue from last time where the Terraform jobs had been defined.
* Add Ansible into CI/CD config.
* Create Ansible deployment Docker image in the GitLab registry 
  * Use Docker-in-Docker CI template prepared for the [main branch](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/51931).
* Kubernetes CI/CD variables, `KUBECONFIG` etc. is only [pre-populated](https://docs.gitlab.com/ee/ci/variables/README.html#deployment-variables) in GitLab managed Kubernetes clusters. 
* GitLab CI/CD: Allow `needs` to refer to a job in the same stage [issue](https://gitlab.com/gitlab-org/gitlab/-/issues/30632).
* Navigate into `CI/CD > Pipelines` and click `Run pipeline` for the `main` branch.

Docker build CI template:

```
docker-build:
  # Use the official docker image.
  image: docker:latest
  stage: build
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  # Default branch leaves tag empty (= latest tag)
  # All other branches are tagged with the escaped branch name (commit ref slug)
  script:
    - |
      job_branch_log_msg="Running job on branch: $CI_COMMIT_BRANCH"
      if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
        tag=""
        echo "$job_branch_log_msg (default branch, latest tag)"
      else
        tag=":$CI_COMMIT_REF_SLUG"
        echo "$job_branch_log_msg (tagging with $tag)"
      fi
    - docker build --pull -t "$CI_REGISTRY_IMAGE${tag}" .
    - docker push "$CI_REGISTRY_IMAGE${tag}"
  rules:
    - if: $CI_COMMIT_BRANCH
      exists: 
        - Dockerfile
```

In the future, we'll explore more Kubernetes topics:

- Use [Renovate](https://fotoallerlei.com/blog/post/2020/automatic-dependency-updates-with-renovate-and-gitlab/post) to keep deployments updated with GitLab CI/CD.
- CI/CD, IaC and GitOps with the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/)
- Hetzner [storage volumes](https://github.com/hetznercloud/csi-driver)
- Monitoring with Prometheus, GitLab CI/CD deployments and much more :) 

### Insights

- [Miro board](https://miro.com/app/board/o9J_lTOCVrg=/)
- [Kubernetes group repos](https://gitlab.com/everyonecancontribute/kubernetes)
- [Repository with the Kubernetes cluster](https://gitlab.com/everyonecancontribute/kubernetes/k3s-demo)
- [Twitter](https://twitter.com/dnsmichi/status/1384903023323922432)

### Recording

Enjoy the session! 🦊 

{{< youtube YiqSj2Qh8sE >}}


