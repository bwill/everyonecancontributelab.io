---
Title: "37. #EveryoneCanContribute cafe: Policy reporter for Kyverno"
Date: 2021-07-07
Aliases: []
Tags: ["kyverno","policy","prometheus","kubernetes","metrics"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

[Frank Jogeleit](https://twitter.com/FrankJogeleit) dives deep into the policy reporter for Kyverno, and how to monitor and view the policy reports in your Kubernetes cluster, with Kyverno as plugin and Loki as log backend integration. 

### Recording

Enjoy the session! 🦊  

{{< youtube 1mKywg9f5Fw >}}

<br>

### Highlights

Frank walked us trough creating a policy report, which is a CRD in Kyverno at the moment. The Kubernetes Policy Prototype WG aims to make this a standard. 

The Policy reporter will be installed with Helm, and needs a configuration file to enable Kyverno, the UI, and Grafana Loki. The UI shows two types of policy reports: Namespace and cluster.

Kyverno is integrated as plugin, and shows the Kyverno policies and more insights. You can use the policy reporter standalone with kubebench for example. 

Question: [Donate the policy reporter to Kyverno itself?](https://twitter.com/dnsmichi/status/1412812527956803586)

[Use the Prometheus Operator](https://twitter.com/dnsmichi/status/1412814174158209025) to the install the monitoring stack with Prometheus and Grafana in Kubernetes, create a custom ServiceMonitor CRD, and open the policy reporter dashboards in Grafana. 

Integrations with [Kube-bench](https://github.com/aquasecurity/kube-bench) integration into the policy reporter UI - [cluster policy reports](https://twitter.com/dnsmichi/status/1412818864572882947). Kubewarden can use the same CRDs, or someone contributes a specific plugin. Follow the Kyverno plugin to map the CRDs, and create a REST API the UI can consume.

### Insights

- [Policy Reporter](https://github.com/fjogeleit/policy-reporter)
  - [Wiki](https://github.com/fjogeleit/policy-reporter/wiki)
  - [Prometheus Operator Integration](https://github.com/fjogeleit/policy-reporter/wiki/prometheus-operator-integration)
  - [Kyverno Plugin](https://github.com/fjogeleit/policy-reporter-kyverno-plugin)
- [Kuberentes Policy Prototypes WG](https://github.com/kubernetes-sigs/wg-policy-prototypes)
- [Twitter thread](https://twitter.com/philip_welz/status/1412791467127578628)
  - [Insight](https://twitter.com/_ediri/status/1412809906206429186)
