---
Title: "18. Cafe: Kubernetes authentication and authorization with user management and RBAC"
Date: 2021-02-24
Aliases: []
Tags: ["gitlab","hetzner","cloud","terraform","ansible","kubernetes"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### Highlights

We are learning how to deploy Kubernetes into Hetzner cloud in this series:

- Provisioned the server and agent VMs with Terraform and Ansible [in the first session](/post/2021-01-27-cafe-14-kubernetes-deployments-to-hetzner-cloud/)
- Deployed [k3s last week](/post/2021-02-03-cafe-15-kubernetes-deployments-to-hetzner-cloud-part-2/) 
- Learned about pods and the [Hetzner load balancer](/post/2021-02-10-cafe-16-kubernetes-deployments-to-hetzner-cloud-part-3/)
- [Ingress controller for load balancer cost savings](/post/2021-02-17-cafe-17-kubernetes-deployments-to-hetzner-cloud-part-4/)

We'll take a break from deploying a Kubernetes cluster this week, and get to know Kubernetes user authentication and authorization from [Niclas Mietz](https://twitter.com/solidnerd).

* Authentication with kubectl in general
* Creating [X509 Client Cert](https://kubernetes.io/docs/reference/access-authn-authz/authentication/#x509-client-certs) for [Authentication](https://kubernetes.io/docs/reference/access-authn-authz/authentication)
* Use of the [X509 Client Cert](https://kubernetes.io/docs/reference/access-authn-authz/authentication/#x509-client-certs)  with kubectl

- Role based access control (RBAC) with Kubernetes [docs](https://kubernetes.io/docs/reference/access-authn-authz/rbac/)
- `Cluster` prefix for role is cluster wide, a role binding is exclusive for a namespace, [docs](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#rolebinding-and-clusterrolebinding).
- Switching the Authentication Strategy from  [X509 Client Cert](https://kubernetes.io/docs/reference/access-authn-authz/authentication/#x509-client-certs) to [OpenID Connect](https://openid.net/connect/)
- Using OpenID tokens, and using Identity Providers (IDPs), [docs](https://kubernetes.io/docs/reference/access-authn-authz/authentication/#openid-connect-tokens)
- [Using GitLab as OpenID Connect identity provider (IdP)](https://docs.gitlab.com/ee/integration/openid_connect_provider.html)
- [Kubernetes Authentication Through Dex as OpenID Proxy](https://dexidp.io/docs/kubernetes/),
- Start to configure the K3s API Server of k3s  with `--oidc` [flags](https://kubernetes.io/docs/reference/access-authn-authz/authentication/#configuring-the-api-server) 
- [Steps for Repeating](https://gitlab.com/everyonecancontribute/kubernetes/k3s-demo/-/blob/add-docs-authn/docs/authn/README.md)

Next week, we'll look into: 

* OpenID Connection of the API Server with Dex and GitLab, continued. 

- Hetzner [storage volumes](https://github.com/hetznercloud/csi-driver)

Future ideas touch monitoring with Prometheus, GitLab CI/CD deployments and much more :) 


### Insights

- [Kubernetes group repos](https://gitlab.com/everyonecancontribute/kubernetes) 
- [Twitter thread](https://twitter.com/dnsmichi/status/1364622431202213893)


### Recording

Enjoy the session! 🦊 

{{< youtube MD2G-2ZkO0E >}}





