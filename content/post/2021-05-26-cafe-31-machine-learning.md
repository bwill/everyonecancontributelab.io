---
Title: "31. #EveryoneCanContribute cafe: Machine Learning for Software Developers (...and Knitters)"
Date: 2021-05-26
Aliases: []
Tags: ["machinelearning","aws","ml","ai"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---


### Recording

Enjoy the session! 🦊  Tip: Join [AWS Summit 2021 EMEA](https://aws.amazon.com/events/summits/online/emea/) on June 9-10, 2021.

{{< youtube -Tk3QpxCoVE >}}

<br>

### Highlights

[Kris Howard](https://twitter.com/web_goddess) from AWS joined us with Machine Learning and Knitting (Michael convinced her giving the talk, after [meeting](https://twitter.com/dnsmichi/status/1391813362657472518) at AWS meetup Nuremberg 2 weeks ago ;)).

Slides: [Machine Learning for Software Developers (...and Knitters)](https://www.slideshare.net/KristineHoward1/machine-learning-for-software-developers-and-knitters)

1. Get training data
2. Label training data
3. Train ML model
4. Deploy ML model
5. Build web frontend for inference 

We went into a live demo with Sagemaker to dive into Rekognition Custom Labels as successor. 

### Insights

- [MIT Neural Invese Knitting](http://deepknitting.csail.mit.edu/) 
- [AWS Sagemaker Ground Truth](https://aws.amazon.com/sagemaker/groundtruth/)
- [AWS Rekognition Custom Labels](https://aws.amazon.com/rekognition/custom-labels-features/)
- [GitLab Tanuki Stan for Issue Triaging](https://gitlab.com/gitlab-org/ml-ops/tanuki-stan)
- [Mitsuba Renderer](https://www.mitsuba-renderer.org/index_old.html)
- [Twitter thread](https://twitter.com/dnsmichi/status/1397584420534726665)




