---
Title: "Policy and Governance for Kubernetes"
Date: 2020-06-23
Aliases: []
Tags: []
Categories: ["Community", "Kubernetes", "Cloud Native"]
Authors: ["nico-meisenzahl"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

Before I talk about Policy and Governance for Kubernetes let’s briefly talk about policy and governance in general. In short, it means to provide a set of rules which define a guideline that either can be enforced or audited. So why do we need this? It is important because in a Cloud ecosystem decisions are made decentralized and also taken at a rapid pace. A governance model or policy becomes crucial to keep the entire organization on track. Those definitions can include but are not limited to, security baselines or consistency of resources and deployments.

So, why do we need Governance and Policy for Kubernetes? Kubernetes provides Role-based Access Control (RBAC) which allows Operators to define in a very granular manner which identity is allowed to create or manager which resource. But RBAC does not allow us to control the specification of those resources. As already mentioned this is a necessary requirement to be able to define the policy boundaries. Some examples are:
* whitelist of trusted container registries and images
* required container security specifications
* required labels to group resources
* permit conflicting Ingress host resources
* permit publicly exposed LoadBalancer services
* …

This is where Policy and Governance for Kubernetes comes in. But let me first introduce you to Open Policy Agent. Open Policy Agent is the foundation for policy management on Kubernetes or even the whole cloud-native ecosystem.

### Open Policy Agent

[Open Policy Agent (OPA)](https://www.openpolicyagent.org/) is an open-source project by [styra](https://www.styra.com/). It provides policy-based control for cloud-native environments using a unified toolset and framework and a declarative approach. Open Policy Agents allows decoupling policy declaration and management from the application code by either integrating the [OPA Golang library](https://www.openpolicyagent.org/docs/latest/integration/#integrating-with-the-go-api) or calling the REST API of a collocated [OPA daemon](https://www.openpolicyagent.org/docs/latest/integration/#integrating-with-the-rest-api) instance.

With this in place, OPA can be used to evaluate any JSON-based inputs against user-defined policies and mark the input as passing or failing. With this in place, Open Policy Agent can be seamlessly integrated with a variety of [tools and projects](https://www.openpolicyagent.org/docs/latest/ecosystem/). Some examples are:
* API and service authorization with [Envoy](https://www.envoyproxy.io/), [Kong](https://konghq.com/kong/) or [Traefik](https://docs.traefik.io/)
* Authorization policies for SQL, [Kafka](https://kafka.apache.org/) and others
* Container Network authorization with [Istio](https://istio.io/)
* Test policies for [Terraform](https://www.terraform.io/) infrastructure changes
* Polices for SSH and sudo
* Policy and Governance for [Kubernetes](https://kubernetes.io/)

![Open Policy Agent](https://dev-to-uploads.s3.amazonaws.com/i/nh10wwfhbn6x4831vnd2.png)

Open Policy Agent policies are written in a declarative policy language called Rego. Rego queries are claims about data stored in OPA. These queries can be used to define policies that enumerate data instances that violate the expected state of the system.

You can use the [Rego playground](https://play.openpolicyagent.org/) to get familiar with the policy language. The playground also contains a variety of examples that help you to get started writing your own queries.

### OPA Gatekeeper — the Kubernetes implementation

[Open Policy Agent Gatekeeper](https://github.com/open-policy-agent/gatekeeper) got introduced by Google, Microsoft, Red Hat, and styra. It is a [Kubernetes Admission Controller](https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/) built around OPA to integrate it with the Kubernetes API server and enforcing policies defined by [Custom Resource Definitions (CRDs)](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/).

The Gatekeeper webhook, gets invoked whenever a Kubernetes resource is created, updated, or deleted which then allows Gatekeeper to permit it. In addition, Gatekeeper can also audit existing resources. Polices, as well as data, can be replicated into the included OPA instance to also create advanced queries that for example need access to objects in the cluster other than the object under current evaluation.

### More details

The below video provides you with deeper insights as well as demos on how to use Open Policy Agent Gatekeeper.

A german version of this talk is available [here](https://www.youtube.com/watch?v=ELl-LN6e_h0).

{{< youtube Z8-AGa5SpUM >}}

