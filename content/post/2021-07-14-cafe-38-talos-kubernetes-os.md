---
Title: "38. #EveryoneCanContribute cafe: Talos, a Kubernetes OS"
Date: 2021-07-14
Aliases: []
Tags: ["talos","os","api","kubernetes"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

Talos is a modern OS designed to be secure, immutable, and minimal. Its purpose is to host Kubernetes clusters, so it is tightly integrated with Kubernetes. Talos is based on the Linux kernel, and supports most cloud platforms, bare metal, and most virtualization platforms. All system management is done via an API, and there is no shell or interactive console.

[Andrew Rynhard](https://twitter.com/andrewrynhard) dives deep into [Talos](https://www.talos.dev/), the Kubernetes OS. 

### Recording

Enjoy the session! 🦊  

{{< youtube hvK0Vj1JZmE >}}

<br>

### Highlights

Getting started with Talos:

- `talosctl cluster create` runs Talos in Docker on your local machine
- Real hardware - ISO to boot up

Talos provides APIs everywhere, and removes the need for SSH.

`talosctl dashboard` brings up a dashboard in the terminal, `talosctl services` shows an overview of the services. 

Supported architectures: x86, amd64, arm64 - AWS EC2 Graviton 

Roadmap and ideas:

- Talos logs are stored in-memory, future vision is to bridge them via the API into fluentd, Loki, etc. 
- Config validation, especially for network parts

[Talos is building](https://twitter.com/dnsmichi/status/1415357483070197764) a UI for logs, events, metrics, overview. 


### Insights

- [Talos website](https://www.talos.dev/)
- [Talos Systems](https://www.talos-systems.com)
- Documentation
  - [Quickstart](https://www.talos.dev/docs/v0.11/introduction/quickstart/)
  - [Getting started](https://www.talos.dev/docs/v0.11/introduction/getting-started/)
- [Repository](https://github.com/talos-systems/talos)  
- [Sidero](https://www.sidero.dev/docs/v0.3/overview/introduction/#overview) for Kubernetes on Bare Metal
- [Cosi](https://github.com/cosi-project) runs on Talos 
- [Twitter thread](https://twitter.com/philip_welz/status/1415341665636732933)


