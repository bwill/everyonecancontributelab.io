---
Title: "18. Kaeffchen: Container registries, Helm Charts, CI/CD efficiency, Update Policies"
Date: 2020-09-16
Aliases: []
Tags: ["docker","container","cicd","monitoring","kubernetes"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

### General

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/37)
- Guests: Bernhard Rausch, Niclas Mietz, Michael Friedrich, Mario Kleinsasser 
- Next ☕ chat `#19`: **2020-09-23** - [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/38) 

### Highlights

- Continued the Docker Hub rate limiting discussion from last week
  - [Current state for GitLab.com](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/11113#note_408416212)
  - [Mirror repositories with skopeo](https://github.com/containers/skopeo)
  - [Events based on new Docker image](https://github.com/crazy-max/diun)
- [Helm chart registry deprecation](https://github.com/helm/charts#deprecation-timeline), use Helm Hub. 
  - [GitLab Helm Charts](https://hub.helm.sh/charts/gitlab/auto-deploy-app)
- [CI Pipeline Efficiency](https://docs.gitlab.com/ee/ci/pipelines/pipeline_efficiency.html)
  - Monitoring - not the tool, find the problem to solve and use cases. Small iterations. 
  - [Practical Monitoring book](https://www.oreilly.com/library/view/practical-monitoring/9781491957349/)
  - Embrace failure culture
  - [Implementing Service Level Objectives](https://www.oreilly.com/library/view/implementing-service-level/9781492076803/)
- Update policies: Define a process and responsibilities

### Recording

Enjoy the session!

{{< youtube L5X5tJsysLY >}}

### Insights

* [GitLab Runner Cache Support for Azure in 13.4](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runnerscacheazure-section)
* [How we upgraded PostgreSQL at GitLab.com](https://about.gitlab.com/blog/2020/09/11/gitlab-pg-upgrade/)
* [GitLab Pipeline Efficiency Documentation (13.4)](https://docs.gitlab.com/ee/ci/pipelines/pipeline_efficiency.html)




