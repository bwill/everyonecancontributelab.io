---
Title: "23. Kaeffchen: Zero Trust"
Date: 2020-10-28
Aliases: []
Tags: ["security","gitlad","boundary"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---


### Highlights

We discussed Zero Trust principles and touched HashiCorp Boundary. 

- [Restrict access to your internal websites on AWS with BeyondCorp
](https://transcend.io/blog/restrict-access-to-internal-websites-with-beyondcorp)
  - Comments are really interesting: https://news.ycombinator.com/item?id=24901699 
- [Zero Trust at GitLab: Implementation challenges (and a few solutions)](https://about.gitlab.com/blog/2019/10/02/zero-trust-at-gitlab-implementation-challenges/)
- [HashiCorp Boundary Introduction](https://learn.hashicorp.com/tutorials/boundary/getting-started-intro)

### Recording

Enjoy the session!

{{< youtube zCXK7BcyvNs >}}


### Insights

- [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/53)
- Guests: [Niclas Mietz](https://twitter.com/solidnerd), [Michael Friedrich](https://twitter.com/dnsmichi), [Tim Meusel](https://twitter.com/BastelsBlog)
- Next ☕ chat `#24`: **2020-11-03** - [Agenda](https://gitlab.com/everyonecancontribute/general/-/issues/59) 

### Bookmarks

- [How I got hacked, lost crypto and what it says about Apple’s security. Part 1](https://ksaitor.medium.com/how-i-got-hacked-lost-crypto-and-what-it-says-about-apples-security-part-1-83c107beae9)
- [Requirements gathering for CI job autoscaling](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27061)







