---
Title: "33. #EveryoneCanContribute cafe: Cloud native security with Snyk"
Date: 2021-06-09
Aliases: []
Tags: ["security","devsecops","cloudnative"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

Develop fast. Stay secure. Find and fix known vulnerabilities in your dependencies.

### Recording

Enjoy the session with [Matt Jarvis](https://twitter.com/mattj_io)! 🦊  

{{< youtube S7E9n0A_SYU >}}

<br>

### Highlights

- Snyk imports the code, runs an initial scan, and then continuously monitors 
  - Code Static Analysis
  - Dockerfile and Kubernetes scans
  - [Infrastructure as Code scans](https://support.snyk.io/hc/en-us/categories/360001342678-Infrastructure-as-code)
- Importance scoring based on CVSS score, is there an exploit available, is there a fix available
- Code suggestions based on OSS project fixes and machine learning
- [VS Code extension](https://github.com/snyk/vscode-extension) and CLI
- Scan images directly on Docker Hub 
- [Snyk Advisor](https://snyk.io/advisor/) to suggest which package as dependency to use 

### Insights

- [Snyk Website](https://snyk.io/)
- [Twitter thread](https://twitter.com/dnsmichi/status/1402658231390965760)






