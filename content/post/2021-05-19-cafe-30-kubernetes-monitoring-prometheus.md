---
Title: "30. #EveryoneCanContribute cafe: Kubernetes Monitoring with Prometheus"
Date: 2021-05-19
Aliases: []
Tags: ["kubernetes","prometheus","monitoring","gitlab","agent"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---


### Recording

Enjoy the session! 🦊 

{{< youtube i2DebnbbAjM >}}

<br>

### Highlights

Niclas walks us through the Prometheus Operator quick start, deploying on the Kubernetes cluster we provisioned with the GitLab Kubernetes Agent thanks to Philip. Prometheus is up and running, deployed with the agent. The next step is to inspect Grafana and find nice default dashboards for the metrics. We tried to use Service Discovery with finding the agent `/metrics` and debugged permissions.  


### Insights

- [Kubernetes Operator Quickstart](https://prometheus-operator.dev/docs/prologue/quick-start/)
- GitLab Kubernetes Agent
  - [Repository](https://gitlab.com/everyonecancontribute/kubernetes/k8s-agent)
  - [/metrics endpoint analysis](https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/merge_requests/263/diffs#8a7842069a72b388bf7db1c9832ea514bbce16b8_19_23)
- [Twitter thread](https://twitter.com/dnsmichi/status/1395056573492367371)
- Prometheus Learning Resources
  - [Trainings by PromLabs](https://training.promlabs.com/)
  - [100 Days of Kubernetes by Anaïs Urlichs](https://100daysofkubernetes.io/observability/prometheus-exporter.html)
  - [Robust Perception Blog](https://www.robustperception.io/blog)
  - [Prometheus: Up and Running book](https://www.oreilly.com/library/view/prometheus-up/9781492034131/)

