---
Title: "35. #EveryoneCanContribute cafe: Litmus - Chaos Engineering for your Kubernetes"
Date: 2021-06-23
Aliases: []
Tags: ["cloudnative","kubernetes","chaosengineering","slo","release"]
Categories: ["Community"]
Authors: ["dnsmichi"]
Featuredimage: "/media/2020/01/01/unikernel.png"
mermaid: true
---

Learn how the CNCF landscape enables Litmus and Chaos Engineering where everyone can benefit from. 

### Recording

Enjoy the session! 🦊  

{{< youtube IiyrEiK4stQ >}}

<br>

### Highlights

- Cloud native pyramid layer dependencies for resilience
- Chaos engineering and SLOs go hand in hand. 
- [Litmus SDK](https://github.com/litmuschaos/litmus-go/tree/master/contribute/developer-guide) for Chaos Engineering development and experiments, standalone from ChaosHub 
- [Custom Resource Definitions (CRDs) for Chaos](https://twitter.com/dnsmichi/status/1407739344924905478)
- [Hub](https://hub.litmuschaos.io/) in airgrapped offline environments - fork the [chaos-charts](https://github.com/litmuschaos/chaos-charts) and use the experiment manifests. 
- [Litmus Chaos 2.x consumes Observability SLI](https://twitter.com/dnsmichi/status/1407745190635376641)
- [GitOps flows for Chaos workflows](https://twitter.com/dnsmichi/status/1407748503372238850)
- Monthly release on the 15th. 
- Integrations
   - GitLab remote templates: https://github.com/litmuschaos/gitlab-remote-templates 
   - Keptn quality gate service: https://github.com/keptn-sandbox/litmus-service

### Insights

- [Litmus Chaos Getting Started documentation](https://docs.litmuschaos.io/docs/getstarted/)
- To join the slack please follow the following steps!
  - Step 1: Join the Kubernetes slack using the following link: https://slack.k8s.io/
  - Step 2: Join the `#litmus` channel on the Kubernetes slack or use this link after joining the Kubernetes slack: https://slack.litmuschaos.io/
- [Principles of Chaos Engineering](https://principlesofchaos.org/)
- [Litmus Chaos Hub](https://hub.litmuschaos.io/)
- [Chaos Carnival Bootcamp demos](https://github.com/chaoscarnival/bootcamps)
- [Twitter thread](https://twitter.com/dnsmichi/status/1407731465509654530)








