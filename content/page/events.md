---
Title: "Events"
Date: 2021-04-26
Authors: ["dnsmichi"]
mermaid: true
---

## Cafe Live Sessions

Please check the calendar events for details on the topics.

If you

- want to demo and promote your new (startup) OSS project,
- dive deep into things you have learned,
- try new technology in a live "pair" programming session,
- ...

then please reach out! Best is to [pitch a new idea](https://gitlab.com/everyonecancontribute/general/-/issues/new?issuable_template=new-idea) or chat on [Twitter](https://twitter.com/i/lists/1297825724976508928). Talk to you soon!

### Calendar

<iframe src="https://calendar.google.com/calendar/embed?src=c_0ttur2of8tt81fpaef4s4jetq4%40group.calendar.google.com&ctz=Europe%2FBerlin" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>